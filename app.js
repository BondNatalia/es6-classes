//1. прототипне наслідування-це коли нащадок наслідує властивості та методи від свого батька
// 2. super визивають для того,щоб  нащадок унаслідував конструктор батька
class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get getName() {
        return this.name;
    }

    set getName(value) {
        this.name = value;
    }

    get getAge() {
        return this.age;
    }

    set getAge(value) {
        this.age = value;
    }

    get getSalary() {
        return this.salary;
    }

    set getSalary(value) {
        this.salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    
    get getSalary() {
        return super.getSalary;
    }

    set getSalary(value) {
        super.getSalary = value * 3;
    }
}

const employee = new Employee("Nick", 30, 40000, "en");
console.log(employee);
const programmer = new Programmer("Linda", 40, 20000, "it")
console.log(programmer);
programmer.getName = "Steve";
programmer.getAge = 35;
programmer.getSalary = 30000;
console.log(programmer);